const mongoose = require ("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		// Requires the data to be included when creating a record
		// The "true" value defines if the field is required or not and the second element in the array is the message that will printed out in our terminal when the data is not present 
		required: [true, "Course is required"]
	},
	description: {
		type: String, 
		required: [true, "Description is Required"]
	},
	price: { 
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	CreatedOn: {
		type: Date,
		// The "new Date ()" expression instantiates a new "date" that the current date and time whenever a course is created in our database
		default: new Date()
	},
	enrollees : [
	{
		userId: {
			type: String,
			required: [true, "UserId is required"]
		},
		enrolledOn: {
			type: Date, 
			default: new Date()
		}
	}]
})
							// model name
module.exports = mongoose.model("Course", courseSchema);


























