const Course = require('../models/course.js');
const mongoose = require('mongoose');
const auth = require('../auth.js');

module.exports.verifyAdmin = (req, res, next) => {
    const newData = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    if (newData.isAdmin == false) {
        return res.send('User must be ADMIN to access this.');
    }

    req.body.newData = newData;

    next();

}

module.exports.addCourse = (reqBody) => {
    let newCourse = new Course ({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });

    return newCourse.save().then((newCourse, error) => {
        if(error) {
            return error;
        }
        else {
            return newCourse;
        }
    })
}



module.exports.getAllCourses = (req, res) => {
    return Course.find({}).then(result => {
        return res.send(result);
    })
}

module.exports.getActiveCourses = (req, res) => {
    return Course.find({isActive: true}).then(result => {
        return res.send(result);
    })
}


module.exports.getCourseById = (req, res) => {
    return Course.findById(req.params.id).then(result => {
        return res.send(result);
    })
}


module.exports.updateCourse = (req, res) => {
    const newData = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    
    const courseId = req.params.id


    if (newData.isAdmin == true) {
        return Course.findByIdAndUpdate(courseId,
        {
            name: newData.course.name,
            description: newData.course.description,
            price: newData.course.price
        },(err, updatedCourse) => {
            if (err) {
                res.send(false);
            }
            
            return res.send(updatedCourse);

        });
    } else {
        let message = Promise.resolve('User must be ADMIN to access this.');
        return res.send('User must be ADMIN to access this.');
    }
}
