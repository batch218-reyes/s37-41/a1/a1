const User = require ("../models/user.js");
const bcrypt = require ("bcrypt");
const auth = require("../auth.js");

module.exports.checkEmailExist = (reqBody) => {
	// ".find" - a mongoose crud operation (query) to find a field value from a collection
	return User.find({email:reqBody.email}).then(result => {
		// condition if there is an existing user
		if (result.length > 0){
			return true;
		}

		// condition if there is no esixting user
		else
		{
			return false;
		}
	})
}

module.exports.registerUser = (req, res) => {

	    let newUser = new User({
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
		// bcrypt - package for password hashing 
		// hashSync - synchronously generate a hash
		// hash - asynchrinously generate a hash
		// hashing - converts a value to another value 
		password:bcrypt.hashSync(reqBody.password, 10),
		// 10 = salt rounds	
		// salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds, the longer 
		mobileNo: reqBody.mobileNo
	})
	return newUser.save().then((user, error) => {
		if(error){
			return res.send(false);
		}
		else{
			return res.send(true);
		}
	})
}



module.exports.loginUser = (res, req) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false;
		}
		else {
			// compareSync is a bcrypt function to compare a hased password to unhashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password do not match
				return false;
				// return "Incorrect password"
			}
		}

	})
}


module.exports.getProfile = (req, res) => {
	return User.findOne({id: reqBody.id}).then(result => {
		if(result == null) {
			return false;
		}
		else {
			return result;
		}
	})
}















