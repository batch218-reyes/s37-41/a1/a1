/*
	npm init -y
	npm i express
	npm i mongoose 
	npm i cors
*/

// dependencies 
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// userRoutes >> index.
const userRoute = require ("./routes/userRoute.js");
const courseRoute = require ("./routes/courseRoutes.js");
// to create an expressserver/application
const app = express();


// Middleware - allows to bridge our backend application(server) to our front end
// allow cross origin resource sharing
app.use(cors());
// to read json object
app.use(express.json());
// to ream forms
app.use(express.urlencoded({extended: true}));
app.use("/user", userRoute);

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.5uymap1.mongodb.net/?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
	});

// Prompts a message once connected
mongoose.connection.once('open', () => console.log('Now connected to Gabriel DB Atlas.'));



// Prompts a message once connected to port 4000
app.listen(process.env.PORT || 4000, () => 
	{console.log(`API is now online on port ${process.env.PORT || 4000 }`)
});


// 3000, 4000, 5000, 8000 - Port numbers for Web applications

